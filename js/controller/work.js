/*  
  @name= index.js
  @author= David Medel Pizarro
  @version= 1.0
  @description= Controller associated to index.html
  @date = 2-10-2019
*/

/*  
  @name= window.onload
  @author= David Medel Pizarro
  @version= 1.0
  @description= Event that captures when the page is loaded.
                Only shows the animation, display all the other elements.
                Calls the function myMove();
  @date = 29-10-2019
  @params= none
  @return = none
*/
window.onload = function() {
    document.getElementById("divAnimateImg").style.display = "block";
    document.getElementById("myHeaderIndex").style.display = "none";
    document.getElementById("divIframe").style.display = "none";
    document.getElementById("divStopImg").style.display = "none";
    document.getElementById("divIndexTable").style.display = "none";
    this.myMove();
};

/*  
  @name= myMove
  @author= David Medel Pizarro
  @version= 1.0
  @description= Event that shows an animation when onload page.
  @date = 29-10-2019
  @params= none
  @return = none
*/
function myMove() {
    var elem = document.getElementById("divAnimateImg");
    var img = document.getElementById("rnaStrand2");
    var pos_x = 400;
    var pos_y = 130;
    var div_height = 460;
    var degree = 360;
    var mov_x = setInterval(frame_x, 5);
    var mov_y = setInterval(frame_y, 20);
    var reduction = setInterval(reduct, 7);
    var rotation = setInterval(rotat, 6.95);
    /*  
        @name= frame_x
        @author= David Medel Pizarro
        @version= 1.0
        @description= This function move the animation in the x axis
        @date = 29-10-2019
        @params= none
        @return = none
      */
    function frame_x() {
        if (pos_x == 0) {
            clearInterval(mov_x);
        } else {
            pos_x--;
            elem.style.left = pos_x + "px";
        }
    }
    /*  
        @name= frame_y
        @author= David Medel Pizarro
        @version= 1.0
        @description= This function move the animation in the y axis
        @date = 29-10-2019
        @params= none
        @return = none
      */
    function frame_y() {
        if (pos_y == 68) {
            clearInterval(mov_y);
        } else {
            pos_y--;
            elem.style.top = pos_y + "px";
        }
    }
    /*  
        @name= reduct
        @author= David Medel Pizarro
        @version= 1.0
        @description= This function reduce the size of the animation
        @date = 29-10-2019
        @params= none
        @return = none
      */
    function reduct() {
        if (div_height == 200) {
            clearInterval(reduction);
        } else {
            div_height--;
            elem.style.width = div_height + "px";
            elem.style.height = div_height + "px";
        }
    }
    /*  
        @name= rotat
        @author= David Medel Pizarro
        @version= 1.0
        @description= This function allows to rotate the animation,
                      and as it is the last function to reach its final state, 
                      we use it to show/hide the rest of elements of the page.
                      calls clickCancel() function
        @date = 29-10-2019
        @params= none
        @return = none
      */
    function rotat() {
        if (degree == 0) {
            clearInterval(rotation);
            document.getElementById("divAnimateImg").style.display = "none";
            document.getElementById("myHeaderIndex").style.display = "block";
            document.getElementById("divStopImg").style.display = "block";
            this.clickCancel();
        } else {
            degree--;
            img.setAttribute("style", "transform:rotate(" + degree + "deg)");
        }
    }
}

/*  
  @name= rotate(event)
  @author= David Medel Pizarro
  @version= 1.0
  @description= This function allows you to rotate the image when it is stopped and
                the mouse is over the div that contains the image.
  @date = 29-10-2019
  @params= event
  @return = none
*/
function rotate(event) {
    var obj = document.getElementById("divStopImg");
    var obj_left = 0;
    var obj_top = 0;
    var xpos;
    var ypos;
    while (obj.offsetParent) {
        obj_left += obj.offsetLeft;
        obj_top += obj.offsetTop;
        obj = obj.offsetParent;
    }
    if (event) {
        //FireFox
        xpos = event.pageX;
        ypos = event.pageY;
    } else {
        //IE
        xpos = window.event.x + document.body.scrollLeft - 2;
        ypos = window.event.y + document.body.scrollTop - 2;
    }
    xpos -= obj_left;
    ypos -= obj_top;
    x = xpos - 100;
    y = ypos - 100;
    degree = -((Math.atan(parseInt(x) / parseInt(y)) * 180) / Math.PI + 45);
    var img = document.getElementById("rnaStrand");
    img.setAttribute("style", "transform:rotate(" + degree + "deg)");
}

/*  
  @name= clickCancel
  @author= David Medel Pizarro
  @version= 1.0
  @description= shows/hide some elements and set tbodyProducts to ""
                and iframeHEader to "Select Product"
  @date = 29-10-2019
  @params= none
  @return = none
*/
function clickCancel() {
    document.getElementById("divIframe").style.display = "block";
    window.frames[0].document.getElementById("divIframeTable").style.display = "block";
    document.getElementById("tbodyProducts").innerHTML = "";
    window.frames[0].document.getElementById("smallText").style.display = "none";
    window.frames[0].document.getElementById("iframeHeader").innerText = "Select Product";
    document.getElementById("divIndexTable").style.display = "none";
}

/*  
  @name= introduceInBBDD
  @author= David Medel Pizarro
  @version= 1.0
  @description= function that show/hides some errors if you don't introduce the correct fields
                in the inputs, if there's no error then open a popup
  @date = 29-10-2019
  @params= none
  @return = none
*/
function introduceInBBDD() {
    var proceedToBBDD = 1;
    var codeType = window.frames[0].document.getElementById("codeType").value;
    var numOfProducts = window.frames[0].document.getElementById("numOfProducts").value;
    for (let i = 0; i < numOfProducts; i++) {
        if (document.getElementById("productName" + i).value == "") {
            document.getElementById("smallTextProduct" + i).style.display = "block";
            proceedToBBDD = 0;
        } else {
            document.getElementById("smallTextProduct" + i).style.display = "none";
        }
        if (codeType == "dnaCode") {
            if (this.isDNASequence(i)) {
                document.getElementById("smallTextDNA" + i).style.display = "none";
            } else {
                document.getElementById("smallTextDNA" + i).style.display = "block";
                proceedToBBDD = 0;
            }
        } else {
            if (this.isProteinSequence(i)) {
                document.getElementById("smallTextProtein" + i).style.display = "none";
            } else {
                document.getElementById("smallTextProtein" + i).style.display = "block";
                proceedToBBDD = 0;
            }
        }
    }
    if (proceedToBBDD == 1) {
        var decision = confirm("Do you really want to introduce this dades ?");
        if (decision) {
            window.open(
                "./popUpWindows/popUpWindow.html",
                "blank",
                "width=800px, height=600px"
            );
        }
    }
}

/*  
  @name= isDNASequence
  @author= David Medel Pizarro
  @version= 1.0
  @description= check if the field is a correct dna sequence or not
  @date = 29-10-2019
  @params= i (the index of the productCode)
  @return = boolean
*/
function isDNASequence(i) {
    var dna = document.getElementById("productCode" + i).value;
    if (dna.search(/[^ATCG]/i) == -1 && dna != "") {
        return true;
    } else {
        return false;
    }
}

/*  
  @name= isProteinSequence
  @author= David Medel Pizarro
  @version= 1.0
  @description= check if the field is a correct protein sequence or not
  @date = 29-10-2019
  @params= i (the index of the productCode)
  @return = boolean
*/
function isProteinSequence(i) {
    var protein = document.getElementById("productCode" + i).value;
    if (protein.search(/[^ARNDCEQGHIULKMFPSTWYVO\*]/i) == -1 && protein != "") {
        return true;
    } else {
        return false;
    }
}